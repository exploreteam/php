#!/usr/bin/env bash

set -e

if [ -n "${DISABLE_CHOWN_FILES}" ]; then
    echo "Not crowing files"
else
    chown -R www-data:www-data /var/www/html
fi

if [ -n "$PROFILING" ] && [ "$PROFILING" = "1" ] ; then
    echo 'Profiling enabled'
    cp /usr/local/etc/php/conf.d/profiling.ini.deactivated /usr/local/etc/php/conf.d/profiling.ini

    if [ ! -f "/xhgui/external/header.php" ]; then
        echo "/xhgui/external/header.php not found - this file is needed for profiling to work";
        exit 1;
    fi
else
    if [ -f "/usr/local/etc/php/conf.d/profiling.ini" ]; then
        rm -f /usr/local/etc/php/conf.d/profiling.ini
    fi
fi

if [ -n "$PHP_USER_ID" ] ; then
    echo "Setting usermod UID ${PHP_USER_ID} on user 'www-data'"
    usermod -u ${PHP_USER_ID} www-data
fi

exec php-fpm "$@"